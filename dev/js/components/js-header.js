(function(){
    $('.js-menu-trigger').on('click', function() {
        const $this = $(this);
        const $mobMenu = $('.js-mobile-menu');
        if ($mobMenu.hasClass('is-opened')) {
            $mobMenu.stop().slideUp('300');
            $this.removeClass('is-active')
            $mobMenu.removeClass('is-opened');
            closeDialog();
        } else {
            $mobMenu.stop().slideDown('300', function(){
                $('html,body').animate(300, 'swing');
            });
            $this.addClass('is-active');
            $mobMenu.addClass('is-opened');
            showDialog();
        }
    });
})();