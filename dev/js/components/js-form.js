(function(){
    $('.js-form-check').on('submit', function() {
        $(this).find('.form-control').each(function() {
            if ($(this).hasClass('error')) {
                return;
            } else {
                $(this).closest('.get-advice__right').addClass('is-submitted')
            }
        });
    });

    // select
    $('.js-select').each(function() {
        const placeholder = $(this).attr('data-placeholder')
        $(this).select2({
            minimumResultsForSearch: -1,
            placeholder: placeholder,
        });
    });
})();