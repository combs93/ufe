(function(){
    let rsPosition = [100, 67, 33, 0];
    let previousClosest = 100;

    let rslider = $('.about-company__rs').roundSlider({
        radius: 220,
        circleShape: "half-bottom",
        sliderType: "min-range",
        showTooltip: false,
        value: 100,
        handleSize: 16,
        width: 2,
        change: function (e) {
            const goal = e.value;
            let closest = rsPosition.reduce(function(prev, curr) {
                return (Math.abs(curr - goal) < Math.abs(prev - goal) ? curr : prev);
            });
            rslider.roundSlider('setValue', closest);


            if (closest === 67) {
                $('.point-second').addClass('is-active');
                $('.point-third').removeClass('is-active');
                $('.point-fourth').removeClass('is-active');
            } else if (closest === 33) {
                $('.point-second').addClass('is-active');
                $('.point-third').addClass('is-active');
                $('.point-fourth').removeClass('is-active');
            } else if (closest === 0) {
                $('.point-second').addClass('is-active');
                $('.point-third').addClass('is-active');
                $('.point-fourth').addClass('is-active');
            }

            if (previousClosest === closest) {
                return;
            } else {
                $('.js-slide-text.is-active').hide().removeClass('is-active');
                $('.js-slide-text[data-text="' + rsPosition.indexOf(closest) + '"]').toggle().addClass('is-active');
                previousClosest = closest;
                rotateCircle();
            }
        },
        update: function(e) {
            if (e.value < 100) {
                $('.about-company__rs-wrap').addClass('about-company__rs-wrap--dragged');
            } else {
                $('.about-company__rs-wrap').removeClass('about-company__rs-wrap--dragged');
            }
        },
        stop: function(e) {
            if (e.value > 99) {
                $('.about-company__rs-wrap').removeClass('about-company__rs-wrap--dragged');
            }
        }
    });
    
    $('.about-company__rs__point').on('click', function() {
        const $index = $(this).attr('data-index');
        const $point = $('.about-company__rs__point');
        const $parent = $('.about-company__rs-wrap');
        
        rslider.roundSlider('setValue', rsPosition[$index]);
        previousClosest === rsPosition[$index];

        $('.js-slide-text.is-active').hide().removeClass('is-active');
        $('.js-slide-text[data-text="' + $index + '"]').toggle().addClass('is-active');

        rotateCircle();

        if (rsPosition[$index] > 67) {
            $parent.removeClass('about-company__rs-wrap--dragged');
        } else {
            $parent.addClass('about-company__rs-wrap--dragged');
        }

        if (rsPosition[$index] === 67) {
            $point.removeClass('is-active');
            $('.point-second').addClass('is-active');
        } else if (rsPosition[$index] === 33) {
            $point.removeClass('is-active');
            $('.point-third').addClass('is-active');
        } else if (rsPosition[$index] === 0) {
            $point.removeClass('is-active');
            $('.point-fourth').addClass('is-active');
        } else {
            $point.removeClass('is-active');
        }
    });

    function rotateCircle() {
        const $el = $('.about-company__circle__image')
        if (!$el.hasClass('is-active')) {
	        $el.addClass('is-active');
	        setTimeout(function () {
	            $el.removeClass('is-active');
	        }, 800);
	    }
    }
})();