(function(){
    if ($('.js-carousel').length) {
        $('.js-carousel').each(function(i, item) {
            const $this = $(this);
            const carousel = new Swiper($this.find('.swiper-container'), {
                direction: 'horizontal',
                slidesPerView: 'auto',
                slidesPerGroup: 1,
                speed: 800,
                loop: false,
                spaceBetween: 20,
                centeredSlides: false,
                // breakpoints: {
                //     768: {
                //         slidesOffsetAfter: 10,
                //         slidesOffsetBefore: 10,
                //         spaceBetween: 15,
                //     },
                //     1190: {
                //         slidesOffsetAfter: 20,
                //         slidesOffsetBefore: 20,
                //         spaceBetween: 20,
                //     }
                // }
            });
        })
    }


    $('.testimonials__item__text').find('span').each(function() {
        // const $fullHeight = $(this).height();

        $(this).closest('.testimonials__item__text').attr('data-height', $(this).height());

        if ($(this).height() > 178) {
            $(this).closest('.testimonials__item__text').addClass('is-hidden');
        }
    });

    $('.read-more').on('click', function() {
        const $parent = $(this).closest('.testimonials__item__text');
        const $fullHeight = $parent.attr('data-height');
        $parent.css('max-height', $fullHeight + 'px');
        $(this).addClass('hidden');
    });
})();