/* frameworks */
//=include ../../node_modules/jquery/dist/jquery.min.js
//=include ../../node_modules/jquery-validation/dist/jquery.validate.min.js

/* libs */
//=include lib/modernizr-custom.js
//=include lib/roundslider.min.js

/* plugins */
//=include ../../node_modules/popper.js/dist/umd/popper.min.js
//=include ../../node_modules/bootstrap/js/dist/util.js
//=include ../../node_modules/bootstrap/js/dist/dropdown.js
//=include ../../node_modules/bootstrap/js/dist/collapse.js
//=include ../../node_modules/select2/dist/js/select2.min.js
//=include ../../node_modules/swiper/js/swiper.min.js

/* separate */
//=include helpers/object-fit.js
//=include helpers/valid.js
//=include separate/global.js

/* components */
//=include components/js-header.js
//=include components/js-form.js
//=include components/js-collapse.js
//=include components/js-roundslider.js
//=include components/js-carousel.js
//=include components/js-map.js


// DISABLE SCROLL ON MOBILE
let scrlTop;

$(window).on('scroll', function() {
    scrlTop = `${window.scrollY}px`;
});

const showDialog = function () {
    const body = document.body;
    let sizeChecker = document.createElement('div');
    let scrollWidth;

    
    sizeChecker.style.overflowY = 'scroll';
    sizeChecker.style.width = '50px';
    sizeChecker.style.height = '50px';
    body.append(sizeChecker);
    scrollWidth = sizeChecker.offsetWidth - sizeChecker.clientWidth;
    
    sizeChecker.remove();
    
    body.style.position = 'fixed';
    body.style.width = '100%';
    body.style.height = '100%';
    body.style.top = `-${scrlTop}`;
    body.style.overflow = 'hidden';
    body.style.paddingRight = scrollWidth + 'px';
};
const closeDialog = function () {
    let body = document.body;
    let scrollY = body.style.top;
    body.style.position = '';
    body.style.top = '';
    body.style.width = '';
    body.style.height = '';
    body.style.overflow = '';
    body.style.paddingRight = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
}
